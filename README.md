This script is for converting a SQLite dump into a format that is readable by MySQL. MySQL differs from SQLite in some painful ways; this helps you get around it.

# Usage

Step 1:
sqlite3 original_database.sqlite3 .dump | python sqlite_to_mysql.py > dumped_data.sql

Step 2:
mysql -p -u root -h 127.0.0.1 test_import --default-character-set=utf8 < dumped_data.sql